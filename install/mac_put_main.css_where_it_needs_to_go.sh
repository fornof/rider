 echo "make sure there's something here"
 size=`cat ../script/main.css`
 if [ -z size ]
 then
  echo "could not find main.css! oh noes! see if you can locate it"
  exit 1 
 fi
 echo "making a directory and installing to ~\projects\writing\script\main.css"
 mkdir -p ~/projects/writing/script/
 cp ../script/main.css ~/projects/writing/script/main.css

