import json
import sys
print(sys.argv)
path = sys.argv[1].replace('\\\\',"\\")
print("path", path)
settings = {}
with open(path, 'r') as f :
    file_load = f.read()
    print("reading...")
    print("fileload",len(file_load)) 
    settings = {}
    if len(file_load) != 0:
        settings = json.loads(file_load)
    settings["markdown-pdf.styles"]=[sys.argv[2]]


with open(path,"w" ) as f:
    f.write(json.dumps(settings))
print("done!")