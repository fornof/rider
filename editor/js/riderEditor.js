var editor, output
let currentId = 1
var selection = '', shifted = false
var enableTrackChanges = true
var currentNode 
let guidSet = new Set([])

let keytags = new Set(["sceneheading", "action", "shot", "character", "parenthetical", "dialogue", "transition", 'page-break'])
document.execCommand('defaultParagraphSeparator', false, 'p');
let tagaroo = "tagged"
let undoStack = []
let ctrlzStack = []
let copyPasteStack
let currentType = "dialogue"
function generateGuid() {
    var result, i, j;
    result = '';
    for(j=0; j<32; j++) {
      if( j == 8 || j == 12 || j == 16 || j == 20) 
        result = result + '-';
      i = Math.floor(Math.random()*16).toString(16).toUpperCase();
      result = result + i;
    }
    // probably unneeded , but in case collision occurs, generate another one
    if(guidSet.has(result)){
       return generateGuid()
    }
    else{
        guidSet.add(result)
    }
    return result;
  }
function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

// Start file download.
function initCurrentNode(){
    let editor = document.getElementById("contentbox")
   let node = editor.firstChild
   console.log("node",node)
   if(node == undefined || node == null){
        let html = tagMaker("transition",false, false)
        let element = $.parseHTML(html)[0]
        editor.appendChild(element)
        console.log("parent?",element.parentNode)
        reinitDraggableSortable()
        console.log("appended Element", editor)
   }
   currentNode  =editor.firstChild
   console.log("currentNode", currentNode)
   return editor.firstChild

}
async function loadFile() {
    console.log("hit handlefile")
    let fileScript = document.getElementById("fileScript")
    let file = await fileScript.files[0].text()
    let inHtml = loadJsonToHtml(file)
    loadHtmlToEditor(inHtml)
}
function loadHtmlToEditor(inHtml) {
    console.log("put in stuff", inHtml)
    editor.innerHTML = inHtml
}

function loadJsonToHtml(jsonText) {
    let lines = JSON.parse(jsonText)
    
    inHtml = ""
    for (line of lines) {
        let guid =  line.id || generateGuid()
        let val = `<div id="${guid}" class="${line.type} ${tagaroo}"><div class="ui-state-default">${line.html}</div></div>`
        inHtml += `${val}`
    }
    return inHtml
}
function saveFile() {
    download("ProfessionalScriptNumber.json", JSON.stringify(saveJson()))
}

function saveJson(parentElement) {
    if (parentElement == undefined) {
        parentElement = editor
    }

    let lines = []
    let elements = parentElement.querySelectorAll('.' + tagaroo)
    elements.forEach(function (element) {
        if (element == null) {
            console.log("is null element")
            return
        }
        line = {}
        line['id'] = element.id
        line['text'] = element.innerText
        line['html'] = element.innerHTML
        for (let classer of element.classList) {
            if (isKeyword(classer)) {
                line['type'] = classer
            }
        }
        lines.push(line)

    });
    let meta= {}
    meta['author'] = "foo" 
    {lines,meta}
    return draft;
}
function selectElementContents(el, removeRanges = true) {
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    if (removeRanges) {
        sel.removeAllRanges();
    }
    sel.addRange(range);
}
function handleSelectElement() {

    //update()
    // take the range , if startcontainer 
    element = document.getSelection().getRangeAt(0).startContainer.parentNode
    console.log("NODE", element)
    selectElementContents(currentNode);
}
function placePageBreaks() {
    // get line count for things, 
    // place page-breaks / calculate page numbers
    // worry about where it breaks later - perhaps keep a "lastdialogue, then break after that. 
    var sum = 0
    let isFirst = true
    $('.page-break').remove()
    let tagged =  $('.tagged')
    let lastTagged = tagged.get(0)
    console.log("last tagged")
    let lastCharacter = $('.tagged .character').get(0)
    let lastDialogue = $('.tagged .dialogue').get(0)
    insertTagAtElement("page-break",tagged, false)
    tagged.each(function (i) {
        let count = getLines(this);
        if (this.classList.contains("character")) {
            lastCharacter = this
        }
        if (this.classList.contains("dialogue")) {
            lastDialogue = this
        }
        if (this.classList.contains("tagged")) {
            lastTagged = this
        }
        if (sum > 20 ) {
             if(lastTagged.classList.contains("dialogue")){
                insertTagAtElement("page-break",lastCharacter, false)
             }
             else{
                insertTagAtElement("page-break",lastTagged, false)
             }
            sum = 0
        }
        sum += count
    })

    setPageNumber()
    update()
}
function loadButtons() {
    let buttons = [
        ["general", "General"],
        ["sceneheading", "Scene Heading"],
        ["action", "Action"],
        ["character", "Character Name"],
        ["parenthetical", "Parenthetical"],
        ["dialogue", "Dialog"],
        ["transition", "Transition"],
        ["shot", "Shot"],
        ["cast-list", "Cast List"],
        ["page-break", "Page Break"]]
    let inHTML = ''
    for (button of buttons) {
        inHTML += `<button onclick="placeTag('${button[0]}')">${button[1]}</button><br>`
    }
    let btnBox = document.getElementById("btnBox")
    btnBox.innerHTML += inHTML
}

function isKeyword(classes) {
    if(!classes){
        return false
    }
    if (typeof classes === typeof "") {
        return keytags.has(classes)
    }
    console.log("classes", classes)
    for (let classer of classes) {
        if (keytags.has(classer)) {
            return true
        }
    }
    return false
}
function cleanAllTagsThatAreEmpty() {
    let children = editor.firstChild
    while (children != undefined) {
        if (children.innerText.length == 0) {
            console.log("Empty:", children)
        }
        children = children.nextSibling
    }
}


function print(message, loc = "#print") {
    $(loc).html(message)
}
function getKeywordElement(element) {
    let inElement = element
    if (element === undefined || element === null || element.id == editor.id) {
        // print("element is undefined for keywordElement")
        return undefined
    }
    do {
        if (element.parentNode != null && element.parentNode.id == editor.id) {
            // console.log("element here", element)
            return element
        }

        element = element.parentNode
        if (element === null) {
            return element
        }
    }

    while (element.id !== "contentbox")
    if (element != null && element.id == "contentbox") {
        print("got to top of contentbox. you'll have to save and reimport ")
        return undefined
    }
    return inElement
}
function setElementInUndoStack(element) {
    let stackPosition = $(element).index()
    undoStack.push([element.cloneNode(true), stackPosition])
}
function addToCtrlZStack(element, fx) {
    return ctrlzStack.push([element, fx])
}
function getElementInUndoStack() {
    return undoStack.pop()
}
function restoreElement(element, stackPosition = 0) {
    let divs = $('#contentbox .tagged')
    if (divs.length > stackPosition) {
        try{
            editor.insertBefore(element, divs.get(stackPosition))
        }catch(error){
            console.log("whoopsies on restoring, let's try the other route")
            editor.insertBefore(element, divs.get(0))
        }
    }
    else {
        editor.insertBefore(element, divs.get(0))
    }
}

function deleteDiv(element) {
    // todo, store the element and position here (like, 5th from the top)
    // save the element in undo
    setElementInUndoStack(element)
    element = getKeywordElement(element)
    if (element) {
        console.log("deleted element", element)
        currentNode = element.nextSibling;
        console.log("Nextcurrent node is", currentNode)
        element.parentNode.removeChild(element);
        return { success: true }
    }
    return { success: false }



}
function clearTexts(element) {
    element = getKeywordElement(element)
    console.log("element is...", element)
    if (element == undefined) {
        return undefined
    }
    let texts = element.getElementsByClassName('ui-state-default')[0]
    texts.innerHTML = texts.innerText
    return 0
}
function getKeywordOrParentElement(element) {
    let inElement = element
    while (!isKeyword(element.classList)) {
        if (element.id === "contentbox") {
            element = inElement
            break
        }
        element = element.parentElement
    }
    console.log("found", element, "returning...")
    return { element }
}
function moveTagsFromElement(element) {
    let startTag = /<(?!\/).+?>/g
    let endTag = /<\/.+?>/g
    let onlyClearTags = false
    // check to make sure its not draft / within bounds here

    //element, onlyClearTags = getKeywordOrParentElement(element)

    if (!onlyClearTags) {
        let tags = element.innerHTML.match(startTag) || []
        console.log("tags1", tags)
        for (tag of tags) {

            if (isKeyword(tag)) {
                console.log("INSERT BEFORE", tag)
                //element.insertBefore(tag)
            }
        }
        tags = []
        tags = element.innerHTML.match(endTag) || []
        console.log("tags2", tags)
        for (tag of tags) {
            if (isKeyword(tag)) {
                console.log("INSERT AFTER", tag)
                //element.insertAfter(tag)
            }
        }
    }
    console.log("htmltotext", element, element.innerHTML)
    //if(isKeyword(element) ){
    element.innerHTML = element.innerText
    //}
}
function tagMaker(type, useWhenDone = true,useDraggable=true) {
    let draggable=""
    let guid = generateGuid()
    if(useDraggable){
        draggable ="draggable"
    }
    if (useWhenDone) {
        return `<div id="${guid}" class="${draggable} ${tagaroo}" when-done="${type}"><div class="ui-state-default">${type}</div></div>`
    }
    return `<div id="${guid}" class="${draggable} ${tagaroo} ${type}"><div class="ui-state-default">${type}</div></div>`
}

function placeTag(type, useWhenDone=true, useDraggable=true) {
    currentType = type
    //console.log("currentNode", currentNode)
    //console.log("selection", selection)
    if (selection.length > 1) {
        var classList = currentNode.classList;
        while (classList.length > 0) {
            classList.remove(classList.item(0));
        }
        currentNode.classList.add(type)
        currentNode.classList.add(tagaroo)
        return
    }
    let val = ''
    //moveTagsFromElement(currentNode)
    // if(type =="page-break"){
    //    setPageNumber()
    // }
    val = tagMaker(type, useWhenDone, useDraggable)
    //let {element} = getKeywordOrParentElement(currentNode)
    let dragElement = document.getElementById("dragElement")
    dragElement.innerHTML = val
    reinitDraggableSortable()

    if (currentNode != undefined && isKeyword(currentNode.classList)) {
        //let element = currentNode.nextSibling.insertBefore()
    }





}
function reinitDraggableSortable(){
    $(".draggable").draggable({
        connectToSortable: page,
        helper: "clone",
        revert: "invalid",
        revertDuration: 100,
        stop: function (event, ui) {
            if (event.target.classList.contains('draggable')) {
                console.log("draggable! eek!", event.target)
                event.target.id = "foo" + currentId++
                event.target.classList.remove("ui-draggable")
                event.target.classList.remove("ui-draggable-handle");
                event.target.classList.remove("draggable");
                event.target.removeAttribute('style');
                console.log("draggable! ah handled!", event.target)
            }
        }
    })
    $(".editor").sortable({
        revert: true,
        start: function (event, ui) {
        },
        stop: function (event, ui) {
            $(ui.item).removeAttr("style");
            let whenDone = $(ui.item).attr('when-done')
            if (whenDone) {
                $(ui.item).addClass(whenDone)
                $(ui.item).removeAttr('when-done')
            }
            $(ui.item).removeClass("draggable")
            if ($(ui.item).hasClass("page-break")) {
                setPageNumber()
            }
        }
    })
}
function setPageNumber() {

    //console.log($(".page-break"))
    let i = 2
    $(".page-break").each(function (index) { if (this.id.slice(0, 3) === "foo") { return }; console.log("breaker", this); this.innerHTML = `<div class="ui-state-default">${i++}. </div>` })

}

function isKeyPressed(event) {

    currentNode = getKeywordElement(event.srcElement) || currentNode 
    if(!currentNode){
        return
    }
    if (currentNode.classList != undefined && currentNode != null && currentNode.classList.contains("draggable")) {
        $("#" + editor.id + " ." + tagaroo).removeClass("draggable").removeAttr("style")
    }




    // if (event.altKey) {
    //     console.log("The ALT key was pressed!");
    //     //likeToServer()
    //     console.log("event!", event)
    //     let result = [event.srcElement, event.srcElement.innerHTML]
    //     console.log(result)
    // } else {
    //     console.log("something else was pressed")
    // }
    console.log("current node", currentNode)

}
window.onload = function () {
    connect()
    editor = document.querySelector('#contentbox');
    output = document.querySelector('#caretposition');
    placeTag("transition",true, true)
    loadButtons()
    let editInner = localStorage.getItem("text")
    if(!editInner || editInner === "" ){
        initCurrentNode()
    }
    else{
        editor.innerHTML =  editInner
    }
    document.getElementById("firstPage").innerHTML = localStorage.getItem("firstPage") || document.getElementById("firstPage").innerHTML
    document.addEventListener('selectionchange', handleSelectionChange);
}
function handleSelectionChange(event) {
    selectioner(event)
    // print(selection)
}

function getCaretPosition(editableDiv) {
    var caretPos = 0,
        sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0);


            // var preCaretRange = range.cloneRange();
            // preCaretRange.selectNodeContents(editableDiv);
            // preCaretRange.setEnd(range.endContainer, range.beginOffset);
            // caretOffset = preCaretRange.toString().length;
            // console.log(preCaretRange.startContainer.innerHTML)

            if (range.commonAncestorContainer.parentNode == editableDiv) {
                caretPos = range.endOffset;
            }
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        if (range.parentElement() == editableDiv) {
            var tempEl = document.createElement("span");
            editableDiv.insertBefore(tempEl, editableDiv.firstChild);
            var tempRange = range.duplicate();
            tempRange.moveToElementText(tempEl);
            tempRange.setEndPoint("EndToEnd", range);
            caretPos = tempRange.text.length;
        }
    }
    return caretPos;
}
function getSectionType(editableDiv) {
    var target = document.createTextNode("\u0001");
    try {
        document.getSelection().getRangeAt(0).insertNode(target);
        console.log(document.getSelection().getRangeAt(0))
    } catch (error) {
        console.log(document.getSelection())
        console.log(error)
        return
    }
    var position = editableDiv.innerHTML.indexOf("\u0001");
    let result = target.parentNode
    target.parentNode.removeChild(target);
    return result
}


var update = function () {
    // if(inSelectionMode){
    //     currentNode = getSectionType(this)
    // }
    // else{
    //     currentNode = document.getElementsByClassName("ui-sortable-helper")[0]
    //     console.log("sort currentnode",currentNode)
    //     print(currentNode.classList,"#current")
    // }

    texts.updateElement(currentNode)
    // update text to global page:
    localStorage.text = editor.innerHTML
    localStorage.firstPage = document.getElementById("firstPage").innerHTML
    //$('#caretposition').html(getCaretPosition(this));
};
function handleCtrlZ() {
    let undoElement = getElementInUndoStack()
    if (undoElement) {
        addToCtrlZStack(undoElement[0], "add")
        restoreElement(undoElement[0], undoElement[1])
    }
}
function insertTagAtElement(type, positionElement, useDraggable=false, isBefore=false) {
    let html = tagMaker(type, false,useDraggable)
    insertHtmlAtElement(html, positionElement, isBefore)
}
function insertHtmlAtElement(html, positionElement, isBefore) {
    let element = $.parseHTML(html)[0]
    insertElementAtElement(element, positionElement, isBefore)
}
function insertElementAtElement(element, positionElement, isBefore) {
    if(isBefore){
        addToCtrlZStack(element,"add")
        editor.insertBefore(element, positionElement)
    }
    else{
        addToCtrlZStack(element,"add")  
        editor.insertBefore(element, positionElement.nextSibling)
    }
    currentNode = element
}
function handleInsert(withAlt = false) {
    if (withAlt) {
        // make a tag OR get the tag currently in place
        // insert into right after currentNode
        insertTagAtElement(currentType, currentNode, false)
        return
    }
    if (!inSelectionMode) {
        editModeClickHandler() // in text area contenteditable= true
        return
    }

    if (currentNode) {
        editModeClickHandler()
        return
    }
}
function handleCtrlV(withShift = false) {
    if (withShift) {
        return
    }
    if (!inSelectionMode) {
        return
    }

    if (currentNode) {
        if(copyPasteStack != undefined ){
            let element = copyPasteStack.cloneNode(true)
            console.log("inserting ctrl v", element)
            
            insertElementAtElement(element, currentNode,true )
        }
       
        return
    }
}
function handleCtrlC(withShift = false) {
    if (withShift) {
        return
    }
    if (!inSelectionMode) {
        return
    }

    if (currentNode) {
        copyPasteStack = undefined
        console.log("inserting ctrl c", currentNode)
        copyPasteStack = currentNode
        return
    }
}
function spanText(){
    val = selection
    if(!val.length){
        // select next character
        //document.(0, 5, "forward");
      //  selectioner()
    }
    //document.execCommand("insertHTML", true, `<span class="delete">${val}</span>`)
}
function handleDelete(event) {
    if (!inSelectionMode) {
       spanText()
       event.preventDefault()
        return
    }

    if (currentNode) {
        deleteDiv(currentNode)
        event.preventDefault()

    }
}
function handleCtrlShiftZ() {
    let undoElement = ctrlzStack.pop()
    if (undoElement) {
        if (undoElement[1] == "add") {
            deleteDiv(undoElement[0])
        }
    }
}

function handleNumbers(keycode, withShift = false) {
    let tag = ""
    if(inSelectionMode){
        
    switch (keycode) {
        case 49:
            // 1
            tag = "character"
            break
        case 50: //2
            tag = "dialogue"
            break
        case 51: //3
            tag = "parenthetical"
            break
        case 52: //4
            tag = "action"
            break
        case 53: //5
            tag = "transition"
            break
        case 54: //6
            tag = "sceneheading"
            break
        case 56 : //7
            saveFile()
            return
        case 55: //8 
        case 48:
        case 57:
            console.log("open keys")
            return
    }    
    placeTag(tag)
    }
}

function handleAllRegularKeys(event){
    
   if(enableTrackChanges){
    val = selection
    if(!val.length){
        // select next character
        //document.(0, 5, "forward");
      //  selectioner()
    }
    //document.execCommand("insertHTML", true, `<span class="adding">${val}</span>`)
}
   
}
function handleKeyStrokes(event) {
    console.log("handling keystrokes", event.key)
    // if(event.key.match(/^[A-Za-z0-9.,-?!@#$%^&\*\(\)_+\\`~\[\]\{\}|]{1}$/g)){
    //     //handleAllRegularKeys(event)   
    // }
    if (event.altKey){
        if (event.keyCode >= 48 && event.keyCode <= 57) {
            handleNumbers(event.keyCode, true)
            return
        }
        if (event.keyCode == 45) { // insert key and ctrl key

            handleInsert(true)
            return
        }
    }
    if (event.ctrlKey) {
    
      
        if (event.shiftKey && event.keyCode == 90) { //ctrl shift z

            handleCtrlShiftZ()
            return
        }
        if (event.keyCode == 90) { // ctrl z

            handleCtrlZ()
            return
        }
        if (event.keyCode == 67) { // ctrl +c 
            // alert("handle ctrl c")
            handleCtrlC()
            return
        }
        if (event.keyCode == 86) { // ctrl +v 
            // alert("handle ctrl v")
            handleCtrlV()
            return
        }


    }
    if (event.keyCode == 45) { // insert key

        handleInsert()
        return
    }
    if (event.keyCode == 46) { // delete key
        //alert("handle del")
        handleDelete(event)
        
        return
    }


}
document.onkeydown = handleKeyStrokes
$('#contentbox').on("mousedown keydown keyup dragend", update);
$('#firstPage').on("mousedown keydown keyup dragend", update);
$('#contentbox').on('mouseup keyup keydown', selectioner);
// $('#contentbox').on('keydown', keyListener);
// function keyListener(e){
//     console.log(e.code, e.which)
//     if (e.code === 'KeyS'){
//         alert("-S pressed");
//         saveFile()
//         event.preventDefault();
//     }
// }
function selectioner(e) {
    if (!inSelectionMode) {
        selection = window.getSelection().toString();
    }
   
}

document.body.addEventListener('click', isKeyPressed, true);
// localstorage
// this is going to fill up 1 localstorage area fast, best to move it to ids/ sections as they come up. 
// todo when scripts get larger than can fill this section. 
function myLocalStorage() {
    console.log("MYSTORAGE HIT")
    let supported = `<div class="transition">fade in.</div>`
    if (window.localStorage) {
        var p = document.getElementById('contentbox');
        if (localStorage.text == null) {
            localStorage.text = p.innerHTML = supported;
        } else {
            p.value = localStorage.text;
        }
        //document.addEventListener('keyup', function () { localStorage.text = p.innerHTML; }, false);
    } else {
        document.getElementById('contentbox').innerHTML = "foo not supported";
    }
}

// Make the DIV element draggable:
//dragElement(document.getElementById("btnBox"));
//dragElement(document.getElementById("sectionBox"));

function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        // if present, the header is where you move the DIV from:
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        // otherwise, move the DIV from anywhere inside the DIV:
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}


function getLines(el) {
    var divHeight = el.offsetHeight
    var lineHeight = parseFloat(getComputedStyle(editor)["lineHeight"])
    var lines = divHeight / lineHeight;
    console.log("Lines: " + lines);
    return lines
}


