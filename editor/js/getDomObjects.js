class DomObjects{
    constructor(){
        this.health = "healthy"
    }
    finder(id){
    
        return document.getElementById(id)
    }
    ready(){
      return  document.readyState === "complete" || document.readyState === "interactive";  
    }
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
    get(id, getText=false){
        
       let element = document.getElementById(id)
       if(!element){
            return false
        }
        if(element.type == "textarea")
        {
            return element.value
        }
        if(getText){
            return element.innerText
        }

            return element.innerHTML
        
    }
    set(id,html){
        let element = this.finder(id)
        if(!element){
            return false
        }
        if(element.type == "textarea")
        {
            element.value = html
        }
        if(element && element.classList.contains("tagged")){
            element.firstChild.innerHTML = html
        }
    }
    addEventListener(type, callback){
        document.body.addEventListener(type,callback)
    }
}
