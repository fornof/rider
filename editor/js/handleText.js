class HandleText{
    constructor({id="foo",DOM=undefined,wsAddress="ws://localhost:3000/test/foobar"}={}){
        
        this.DOM = DOM 
        this.id = id
        this.wsAddress = wsAddress
        this.socket
        
        this.update = this.updateElement.bind(this)
        this.send = this.sendToWS.bind(this)
        this.connect = this.connect.bind(this)
        this.handleData.bind(this)
    }
    connect(address){
        this.socket = new WebSocket(address || this.wsAddress);
        this.registerSocketEvents()
    }
    registerSocketEvents(){
        this.socket.onopen = this.onOpen.bind(this);
        this.socket.onclose = this.onClose.bind(this);
        this.socket.onerror = this.onError.bind(this);
        this.socket.onmessage = this.onMessage.bind(this);
    }
    onOpen(e){
            console.log("[open] Connection established");
            console.log("Sending to server");
            
    }
    handleData(jsonData){
        console.log("from",jsonData.from ,"id", this.id)
        if(jsonData.from == this.id){
            return false
        } 
        console.log("jsondata",jsonData)
        this.DOM.set(jsonData.id,jsonData.html) 
        return true
    }
    onMessage(event){
        try{
            if(this.handleData(JSON.parse(event.data))){
                console.log("handled Data")
            }
        }catch(error){
                console.log("could not parse handleText data", error)
            }
        // console.log(`[message] Data received from server: ${event.data}`);
    }
    onClose(){
        if (event.wasClean) {
            console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
          } else {
            // e.g. server process killed or network down
            // event.code is usually 1006 in this case
            console.log('[close] Connection died');
          }
        
    }
    onError(error){
        console.log(`[error] ${error.message}`);
    }
    log(message){
        console.log(message)
    }
    registerEventListener(element){
        if(element){
            element.onkeyup = this.update
            element.onkeydown = this.update
        }
    }
    unregisterEventListener(element){
        if(element){
        element.onkeyup = undefined
        element.onkeydown = undefined
        }
    }
    updateElement(element){
        console.log("hit updateFIrst")
        if(element && element.classList.contains("tagged")){
            this.sendToWS(JSON.stringify({id:element.id, html:element.firstChild.innerHTML}))
        }
    }
    sendToWS(message){
        this.socket.send(message)
    
    }

}
console.log("new dom",)

let texts 
async function connect(id=generateGuid()){
    let DOM = new DomObjects()
    //let id = await DOM.get('id', true)
    console.log("id,",id)
    let wsAddress="ws://localhost:3000/test/"+id
    texts = new HandleText({id,DOM,wsAddress})
    texts.connect()
    texts.registerEventListener(currentNode)
}
