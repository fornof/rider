var inSelectionMode = true
let page = ".editor"
function onDragStart(event) {
    event
        .dataTransfer
        .setData('text/plain', event.target.id);
}
function onDragStart(event) {
    event
        .dataTransfer
        .setData('text/plain', event.target.id);

    event
        .currentTarget
        .style
        .backgroundColor = 'yellow';
}
function onDragOver(event) {
    event.preventDefault();
}
function onDrop(event) {
    const id = event
        .dataTransfer
        .getData('text');

    const draggableElement = document.getElementById(id);
    const dropzone = event.target;

    dropzone.appendChild(draggableElement);

    event
        .dataTransfer
        .clearData();
}

$(page).sortable({
    revert: true,
    start: function( event, ui ) { 
        //$(ui.item).addClass("");
      },
      stop:function( event, ui ) { 
        $(ui.item).removeAttr("style");
        $(ui.item).removeClass("draggable")
      }
  
})
// .click(function () {
//     if ($(this).is('.ui-draggable-dragging')) {
//         return;
//     }
//     $(this).draggable("option", "disabled", true);
//     $(this).attr('contenteditable', 'true');
// })
//     .blur(function () {
//         $(this).draggable('option', 'disabled', false);
//         $(this).attr('contenteditable', 'false');
//     });;
function editModeClickHandler(){
   
        let items = `${page} .ui-state-default`
        // if ($(page).is('.ui-draggable-dragging')) {
        //     return;
        // }
        if (inSelectionMode) {
            $(page).sortable("option", "disabled", true);
            $(items).attr('contenteditable', 'true');
            console.log(inSelectionMode+" editable: and making contenteditabletrue", items, page)
            inSelectionMode = false
        }
        else {
           $(page).sortable('option', 'disabled', false);
            $(items).attr('contenteditable', 'false');
            console.log(inSelectionMode+" editable false: and making sortable disabled", items, page)
            inSelectionMode = true
        }
    
}
$(".droppable").droppable({
    activeClass: "ui-state-hover",
    hoverClass: "ui-state-active",
    accept: ":not(.ui-sortable-helper)",
    drop: function(event, ui) {
        var dropId = ui.draggable.attr("id");
        var targetCount = $(this).find("[id*='clone']").length;
        var $dropElemClone = ui.draggable.clone();
        console.log("droppable handling")
        $dropElemClone
          .attr("id", dropId + "-clone-" + (targetCount + 1))
          .appendTo(this)
          .removeClass("draggable");
      }
    })
//$('#btnToggleEdit').click(EditModeClickHandler())
function intitiateDraggable(){
$(".draggable").draggable({
    connectToSortable: page,
    helper: "clone",
    revert: "invalid",
    stop: function(event, ui){
        if ($(this).hasClass('draggable')){
            console.log("draggable! eek!")
            $(this).removeClass('draggable');
            $(this).removeAttr('style');
        }           
    }
})
}


