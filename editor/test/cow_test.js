#!javascript
var expect = chai.expect;

describe("Cow", function() {
    var sandbox;
      
   
  
    afterEach("do stuff",function() {
      // restore the environment as it was before
     
    });
  
    // ...
  describe("constructor", function() {
    it("should have a default name", function() {
      var cow = new Cow();
      expect(cow.name).to.equal("Anon cow");
    });

    it("should set cow's name if provided", function() {
      var cow = new Cow("Kate");
      expect(cow.name).to.equal("Kate"); 
    });
  });

  describe("#greets", function() {
    it("should throw if no target is passed in", function() {
       console.log(sinon)
        // sandbox = sinon.createStubInstance();
        var callback = sinon.fake();
        var proxy = once(callback);
    
        proxy();
        
    //   sandbox.stub(window.console, "log");
    //   sandbox.stub(window.console, "error");
      expect(function() {
        (new Cow()).greets();
      }).to.throw(Error);
      sandbox.restore();
    });

    it("should greet passed target", function() {
      var greetings = (new Cow("Kate")).greets("Baby");
      expect(greetings).to.equal("Kate greets Baby");
    });
  });
});