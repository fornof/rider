# About this code: 
I want a free script writer for making movie and commercial scripts. 
- I was going to use Celtx, but they went near full commercial and made it hard to install the free version or find the link. 
- Trelby looks like they stopped their production and I can't find the link for the mac version.  
- The alternatives to Final Draft are pay or subscription based, and as a principle, I make a habit always exhaust the free option before moving to the paid option. 

Version control is also an issue. It looks like Final Draft has some form of it, but importing/exporting/reformatting issues seem to occur ever so often. Microsoft Word has a version control of sorts, but I have used it and it is not comparable to SVN or git which I use in production for software. 

# todo :
#next step: 
    read the fdx file, and spit out Type | value
    if nested xml, append out >> 

# todo 
- put in script converter / associated commands
- script is fdx - put in a demo fdx file. 
