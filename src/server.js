const Koa = require('koa'),
  route = require('koa-route'),
  websockify = require('koa-websocket');
const serve = require('koa-static');
const mount=require('koa-mount')

const app = new Koa();
const appWS = websockify(new Koa());

function startWebsocket() {
  // Regular middleware
  // Note it's app.ws.use and not app.use
  console.log("startWs")
  appWS.ws.use(function (ctx, next) {
    // return `next` to pass the context (ctx) on to the next ws middleware
    return next(ctx);
  });

  // Using routes
  let lastMessage ="{}"
  appWS.ws.use(route.all('/test/:id', function (ctx, id) {
    // `ctx` is the regular koa context created from the `ws` onConnection `socket.upgradeReq` object.
    // the websocket is added to the context on `ctx.websocket`.
    ctx.websocket.send(lastMessage);
    ctx.websocket.on('message', function (message) {
      // do something with the message from client
      // ctx.response= new Response({"foobar":"foobar"})
      let outputMsg
      try {
        outputMsg = JSON.stringify({ from: id, ...JSON.parse(message) })
      }
      catch (error) {
        console.log("errorParsingSendMessage", error)
        outputMsg = id + ":" + message
      }
      broadcast(outputMsg);
      lastMessage = outputMsg
      console.log(outputMsg);
    });
  }));

  appWS.listen(3000);
  console.log("listening 3000")
}

function broadcast(message, wsapp = appWS) {
  console.log("hit broadcast", wsapp.ws.on)
  wsapp.ws.server.clients.forEach(function each(client) {
    client.send(message);
  });
}

function startWebServer() {
  app.use(mount('/', serve('editor/html')))
  app.use(mount('/js', serve('editor/js')));
  app.use(mount('/css', serve('editor/css')));
  app.listen(3030)
  console.log("listening 3030")

}
class Response {
  constructor(data) {
    this.data = data
  }
  toJSON() {
    return JSON.stringify(this.data)
  }
}
function main() {
  startWebsocket()
  startWebServer()
}
main()